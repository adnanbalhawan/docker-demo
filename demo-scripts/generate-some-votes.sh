#!/bin/sh

# this short script will post some HTTP data to the Voting app to generate votes
# this makes for a better demo and if you can see the results then you know the solution works

host_header=vote.dogvs.cat
end_point=http://127.0.0.1:80
no_proxy=127.0.0.1,vote.dogvs.cat

while true; do result=$(cat /dev/urandom | tr -dc 'ab' | fold -w 1 | head -n 1); curl -ksv -o /dev/null -X POST -H "Host: $host_header" -H "Content-Type: application/x-www-form-urlencoded" -d "vote=$result" $end_point; done
