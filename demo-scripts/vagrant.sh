#!/bin/bash
# To be run in Windows git bash terminal

export http_proxy=http://fastweb.int.bell.ca:80/
export https_proxy=http://fastweb.int.bell.ca:80/

vagrant plugin install vagrant-proxyconf

cd ../deploy-local-vagrant
vagrant up

vagrant status

echo "DONE! Vagrant has finished starting the VMs"
