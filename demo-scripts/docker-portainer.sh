#!/bin/bash
# To be run on the swarmlead1 VM as centos user

if [ "$USER" != centos ]; then
    echo "This script should be run as centos user."
    exit 1;
fi

docker node ls

cd ../docker-stack-files
docker stack deploy -c portainer-stack.yml portainer

sleep 10;
docker service ls

# Prep for Traefik
docker network create --driver overlay traefikExt

echo -e "\n"
echo "Wait until all services are fully up"
echo "Run: 'docker service ls'"
echo ""
echo "Open Web browser to access Portainer: http://127.0.0.1:9000/"
echo ""
echo "Once finished setting up Portainer you can use reach Portainer through Traefik: https://127.0.0.1:8443/portainer"
echo "And your app from: https://127.0.0.1:8443/app_url"
