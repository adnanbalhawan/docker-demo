#!/bin/bash
# To be run on the ansible VM as ansible user

if [ "$USER" != ansible ]; then
    echo "This script should be run as ansible user."
    exit 1;
fi

cd ../ansible

# Creating your temp .vaultpass since we have it setup by default, but didn't encrypt anything..
echo "RANDOM PASSWORD HERE #TODO Change" | sudo tee .vaultpass

# Fix for Vagrant shared file system as root and symbolic links
sudo rm hosts-sandbox
sudo ln -s inventories/sandbox-openstack/hosts hosts-sandbox

# For Demo purposes we added ssh password to the command line. This is a bad idea since it's now in your history. Remove ansible_ssh_pass parameter normally, and add --ask-pass
echo "Running Ansible Playbook: create-user-ansible.yml"
ansible-playbook create-user-ansible.yml --become --become-method=sudo --extra-vars "inventory=swarm-hosts ansible_ssh_user=centos ansible_ssh_pass=Password123321123321! username=ansible" -i inventories/sandbox-openstack/hosts

echo "Running Ansible Playbook: new-server-swarm.yml"
ansible-playbook new-server-swarm.yml --extra-vars "inventory=swarm-hosts" --extra-vars '{"chrony_ntpservers":["bell.corp.bce.ca"]}' -i inventories/sandbox-openstack/hosts

echo "Running Ansible Playbook: install-dockerV2.yml"
ansible-playbook ./roles/install-dockerV2/install-dockerV2.yml --extra-vars "swarm_iface=eth1" -i hosts-sandbox

echo "DONE! Your Docker Swarm is ready to go"
