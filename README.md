# Geekfest Demo: Building a Docker Tech Stack on Anything*

This demonstration will provide the source code and context on how to build a simple cloud agnostic container platform using minimum hardware requirements. The platform will be able to provide container orchestration (Docker Swarm), layer seven load balancing and SSL offload (Traefik), centralized logging (ELK/ELG) and monitoring (Promethius/InfluxDB/Grafana) and container health checks.
The ability to deploy such a stack on any resources available (Bell laptops, servers or IaaS provider) allows a team to focus on other challenges in their transformation. This model also helps build individual proficiency and creativity by allowing room to experiment, innovate and fail faster with minimal blast radius.

Choose your own adventure.. Build on your local laptop, or in the OpenStack cloud.  
The same demo can also work on bare metal servers.  

Powerpoint can be found on [Confluence](https://confluence.wnst.int.bell.ca/display/GeekFest/2019+GeekFest+Sessions) under "Building a Docker Tech Stack on Anything by Paul Faiazza and Ryan Daniels"  

## Assumptions

We are using a standard Bell issued Windows 10 laptop. Vagrant should be the same on Mac or Linux.  

## Security / Disclaimer

* Do not try this in production!
* Don’t use passwords to log into your VMs, use ssh keys. Change all the passwords in this demo
* Never put production traffic on OpenStack floating IPs
* 1 manager isn’t enough (redundancy)
* Use security tools to scan public Docker images

## Considerations / Limitations

### Laptop for Local VMs

A few things to consider when using your laptop:  

* [ ] Enable VT-x (Intel virtualization) in your laptop BIOS. Open a request from [My Telecom Warehouse](http://cs.qc.bell.ca/Login.aspx) then open a request with CGI and bring your laptop to the depot.
* [ ] Do you have a decent laptop (with a decent CPU, enough RAM (memory), free disk space) ?
* [ ] Do you ever use BELL1 wifi? This will be a problem due to Symantec Endpoint Protection. More on this below.

#### BELL1 wifi and Symantec Endpoint Protection

Symantec Endpoint Protection (SEP) does not like networks being "bridged" together. When on BELL1 wifi, SEP will detect a your VM network as a wired connection. And shut off your wireless connection. If you need BELL1 wifi (like when you need to go into a meeting room), you must disable the network adapters for any VM software. Example: "VirtualBox Host-Only Ethernet Adapter"

No VMs will work without the VM network adapter. So you cannot start or use any VMs when you are on BELL1 wifi. But if you don't disable the VM network adapter, BELL1 wifi will not work at all due to Symantec Endpoint Protection.

For this reason we use Vagrant (via VirtualBox), since it is easy enough to disable the VM network adapter.  

You can try to disable SEP temporarily, but it likes to turn back on. And monitoring Windows Events for this is too slow. You will always be disconnected for 1-3 minutes and lose all your connectivity.  

##### Disable Network Adapter when using BELL1 wifi

In Windows, go to "Network Connections" and disable "VirtualBox Host-Only Ethernet Adapter"

#### Docker for Windows

Docker for Windows is an option for you if you don't need to use BELL1 wifi. Docker for Windows uses Hyper-V, which is not easy to quickly turn off and back on (requires rebooting).

During testing, Hyper-V did not play nice with our VPN client. There was constant disconnect/re-connecting randomly happening.

### OpenStack Cloud VMs

* [ ] Connectivity issues possible. You might need to open a firewall request to connect to the floating IP. ("Publicly" accessible from Bell network). Anywhere you need to connect from, will need to have access (Bell1 wifi, wired, VPN, lab network, etc).. wherever your laptop will be.
* [ ] Use of floating IP - can't be used for anything mission critical (production). You'll need to add an external network or external load balancer.

## Usage

Take your pick, laptop or cloud (OpenStack) for a dev environment.  

### Build

#### Local VMs

Build VMs using Vagrant and VirtualBox. Go to:

* [deploy-local-vagrant](deploy-local-vagrant)

#### Cloud VMs

Build VMs using OpenStack, in the "cloud". Go to:

* [deploy-openstack-tf](deploy-openstack-tf)

### Configure

Then, Install/Configure the VMs. Go to:

* [ansible](ansible)

### Docker Swarm

Lastly, Deploy Docker Swarm Stack files. Go to:

* [docker-stack-files](docker-stack-files)
