# Geekfest Demo - Docker Swarm Setup and Stack files

Setup your Docker Swarm, using [Portainer](https://portainer.readthedocs.io/en/stable/deployment.html#inside-a-swarm-cluster) and [Traefik](https://docs.traefik.io/).

## Usage

### Using OpenStack

- From the ansible control server, ssh to swarm leader (from ansible user)

```bash
[centos@ansible-01 ~]$ sudo su - ansible
[ansible@ansible-01 ~]$ ssh 192.168.100.100

[ansible@swarmlead-01 ~]$ sudo su - centos
```

- OR, From your laptop you can ssh through the Ansible server to the Swarm leader:

```bash
ssh -i ~/.ssh/id_rsa -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -tL 9000:127.0.0.1:9000 centos@xxx.xxx.xxx.xxx

#If you don't expose swarmlead1 to a floating IP, you need to go through your ansible jump box:
#ssh -i ~/.ssh/id_rsa -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -tL 9000:127.0.0.1:9401 centos@xxx.xxx.xxx.xxx "sudo -u ansible ssh -tL 9401:127.0.0.1:9000 ansible@192.168.100.100"

sudo su - centos
```

Change IP xxx.xxx.xxx.xxx to your floating IP assigned to the Ansible control server.  
The first port 9000 can be anything you like. On your local laptop you can use a browser to reach port 9000 on the Swarm leader (where we will setup Portainer next).  
This assumes you have your SSH key (previously added in OpenStack web dashboard), at "~/.ssh/id_rsa".  

### Using Vagrant

- ssh to the swarm leader

From Git Bash, run:

```bash
vagrant ssh swarmlead1
sudo su - centos
```

### Setup Docker Swarm Stacks

- Download the project

```bash
git clone https://git.ctslab.int.bell.ca/wnc/geekfest-2019-docker-tech-stack.git demo-docker-tech-stack
```

- From the Docker Swarm leader (192.168.100.100), check the status of the Swarm nodes:

```bash
docker node ls
```

- Add a Docker Swarm Stack file and deploy Portainer:

```bash
cd demo-docker-tech-stack/docker-stack-files

docker stack deploy -c portainer-stack.yml portainer

docker service ls

docker service ps portainer_portainer

docker service logs portainer_portainer
```

- Using a web browser, you can now connect directly to the Portainer web gui at: <http://127.0.0.1:9000/>

  Create admin user.  
  Select "Agent"  
  Name: sandbox-openstack  
  Agent URL: tasks.portainer_agent:9001

- Add Traefik network to Docker Swarm:

```bash
docker network create --driver overlay traefikExt
```

- Deploy Traefik Stack to Docker Swarm:

  In Portainer, go to: Stacks -> Add Stack  
  Name: traefik  
  Web editor: Paste in the Traefik stack file  
  Click "Deploy Stack"  

- Disconnect from your previous ssh connection, and reconnect to the Traefik port (443). Now all traffic into Swarm will be routed using the Traefik endpoints

  For OpenStack only, if using Vagrant skip this step since we already exposed these ports in the Vagrantfile!

  If there's a firewall issue and you can only ssh to your floating IP, From now on use this to get to your applications:

```bash
ssh -i ~/.ssh/id_rsa -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -tL 8443:127.0.0.1:443 centos@xxx.xxx.xxx.xxx

#If you don't expose swarmlead1 to a floating IP, you need to go through your ansible jump box:
#ssh -i ~/.ssh/id_rsa -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -tL 8443:127.0.0.1:9402 centos@xxx.xxx.xxx.xxx "sudo -u ansible ssh -tL 9402:127.0.0.1:443 ansible@192.168.100.100"
```

- Using a web browser, you can now connect to the Portainer web gui at: <https://127.0.0.1:8443/portainer>

  Other applications can be reached from <https://127.0.0.1:8443/app_url> as defined in your Traefik config via stack files

  Or use the floating IP to reach your swarm leader: <https://xxx.xxx.xxx.xxx:/portainer>

- Use Portainer to deploy any other application Docker Swarm Stacks

- Some stacks are easier to deploy via command line, because they have config files. But that means you can't edit it from Portainer.

```bash
docker stack deploy -c tig-stack.yml tig
docker stack deploy -c elk-stack.yml elk
```

## Misc Tips

### Portainer Web GUI can't talk to the Portiner Agents

Sometimes if the swarm leader is restarted, portainer loses connectivity to the agents. To fix, manually on swarmlead01:

```bash
docker service update portainer_portainer_agent --force
```

Then on Portainer "Home" page, click button "Refresh". And reload the page.
