# ansible-role-check-rhel

CentOS7 / RHEL7 - Check server config


Example Playbook check-rhel.yml
------------

```
---
- hosts: '{{inventory}}'
  become: no
  roles:
  - check-rhel
```


Default Settings
------------

```
debug_enabled_default: false
server_check_ntp_managed: true
server_check_firewall_managed: true
server_check_email_managed: true
server_check_certs_managed: true
install_cert_destination: /etc/pki/ca-trust/source/anchors
server_check_pkg_managed: true
#space separated list of package names
server_check_pkg_names: docker-ce mongodb-enterprise 
server_check_kernel_managed: true
server_check_root_password_set: true
```


Usage
------------

Check RHEL server config:
```
ansible-playbook check-rhel.yml --extra-vars "inventory=centos7" -i hosts-dev
```

Check RHEL server config (only Ansible vars):
```
ansible-playbook check-rhel.yml --extra-vars "inventory=centos7" -i hosts-dev --tags=debug_info
```

Check RHEL server config (only NTP related):
```
ansible-playbook check-rhel.yml --extra-vars "inventory=centos7" -i hosts-dev --tags=check_ntp
```

Check RHEL server config (only NTP related with less output):
```
ansible-playbook check-rhel.yml --extra-vars "inventory=centos7" -i hosts-dev --tags=check_ntp_failure
```

Check RHEL server config (only firewall related):
```
ansible-playbook check-rhel.yml --extra-vars "inventory=centos7" -i hosts-dev --tags=check_firewall
```

Check RHEL server config (only email related):
```
ansible-playbook check-rhel.yml --extra-vars "inventory=centos7" -i hosts-dev --tags=check_email
```

Check RHEL server config (only custom certificate related):
```
ansible-playbook check-rhel.yml --extra-vars "inventory=centos7" -i hosts-dev --tags=check_certs
```

Check RHEL server config (only specified package's version):
```
ansible-playbook check-rhel.yml --extra-vars "inventory=centos7" -i hosts-dev --tags=check_pkgs
```

Check RHEL server config (only kernel issue related):
```
ansible-playbook check-rhel.yml --extra-vars "inventory=centos7" -i hosts-dev --tags=check_kernel_failure
```

Check RHEL server config (only root password related):
```
ansible-playbook check-rhel.yml --extra-vars "inventory=centos7" -i hosts-dev --tags=check_root_password_set
```


## Misc OS commands:
```

# get all Ansible internal variables:
ansible centos7 -m setup -i hosts-dev

# Show systemd status of specific items
systemctl show -p UnitFileState -p SubState chronyd

# Chrony time sync info
chronyc tracking;chronyc sources;chronyc sourcestats
```
