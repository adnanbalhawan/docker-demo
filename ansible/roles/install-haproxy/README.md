# ansible-role-install-haproxy

RHEL7 - Install and configure haproxy from the RHEL Software collections repo.  
HAProxy Version 1.8


## Example Playbook install-haproxy.yml

```
---
- hosts: '{{inventory}}'
  become: yes
  roles:
  - install-haproxy
```


## Default Settings

```
debug_enabled_default: false
install_haproxy_pkgs: 
  - rh-haproxy18-haproxy
  - rh-haproxy18-haproxy-syspaths
install_haproxy_config_dir: /etc/opt/rh/rh-haproxy18/haproxy
install_haproxy_cert_dir: /etc/pki/tls/certs
install_haproxy_pem_dir: /etc/haproxy/certs
install_haproxy_key_dir: /etc/pki/tls/private
install_haproxy_cert_files:
  - file.cert
install_haproxy_pem_files:
  - file.pem
install_haproxy_key_files:
  - file.key
```


## Usage

Install and configure haproxy:
```
ansible-playbook install-haproxy.yml --extra-vars "inventory=rhel7" -i hosts-dev
```

Only install:
```
ansible-playbook install-haproxy.yml --extra-vars "inventory=rhel7" -i hosts-dev --tags install
```

Only update haproxy config and reload haproxy:
```
ansible-playbook install-haproxy.yml --extra-vars "inventory=rhel7" -i hosts-dev --tags config
```

Only remove proxy vars from /etc/profile:
```
ansible-playbook install-haproxy.yml --extra-vars "inventory=rhel7" -i hosts-dev --tags config_etcprofile
```

Only copy custom error templates:
```
ansible-playbook install-haproxy.yml --extra-vars "inventory=rhel7" -i hosts-dev --tags copy_error
```

Only copy certificates:
```
ansible-playbook install-haproxy.yml --extra-vars "inventory=rhel7" -i hosts-dev --tags copy_cert
```

Only copy file needed for new certificate generation:
```
ansible-playbook install-haproxy.yml --extra-vars "inventory=rhel7" -i hosts-dev --tags ssl_config
```

## Misc OS commands

```
# Generate SAN config file for Openssl self signed cert
cat << EOF > /etc/pki/tls/san.conf
[ req ]
default_bits       = 2048
default_keyfile    = san.key #name of the keyfile
distinguished_name = req_distinguished_name
req_extensions     = req_ext

[ req_distinguished_name ]
countryName                 = Country Name (2 letter code)
countryName_default         = GB
stateOrProvinceName         = State or Province Name (full name)
stateOrProvinceName_default = West Midlands
localityName                = Locality Name (eg, city)
localityName_default        = Birmingham
organizationName            = Organization Name (eg, company)
organizationName_default    = Example
commonName                  = Common Name (e.g. server FQDN or YOUR name)
commonName_max              = 64

[ req_ext ]
subjectAltName = @alt_names

[alt_names]
DNS.1   = www.example.com
DNS.2   = www.example.net
DNS.3   = www.example.org
EOF

# Generate self signed cert (on RHEL)
openssl req -x509 -nodes -days 3650 -newkey rsa:2048 -sha256 -subj "/C=CA/ST=Ontario/L=Toronto/O=Bell Mobility/OU=WNC/CN=yourserver.int.bell.ca" -reqexts SAN -extensions SAN -config <(cat /etc/pki/tls/san.conf <(printf "[SAN]\nsubjectAltName=DNS:yourserver.int.bell.ca,IP:127.0.0.1")) -keyout /etc/pki/tls/private/haproxy_F5_hostname_https.key -out /etc/pki/tls/certs/haproxy_F5_hostname_https.cert
chmod 400 /etc/pki/tls/private/haproxy_F5_hostname_https.key
chmod 600 /etc/pki/tls/certs/haproxy_F5_hostname_https.cert

cat /etc/pki/tls/certs/haproxy_F5_hostname_https.cert /etc/pki/tls/private/haproxy_F5_hostname_https.key | tee /etc/haproxy/certs/haproxy_F5_hostname_https.pem
chmod 400 /etc/haproxy/certs/haproxy_F5_hostname_https.pem

# Verify cert:
cat /etc/pki/tls/certs/haproxy_F5_hostname_https.cert | openssl x509 -inform pem -noout -text
```
