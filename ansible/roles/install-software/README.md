# ansible-role-install-software

CentOS7 / RHEL7 - Install specified software packages

## Example Playbook check-rhel.yml

```
---
- hosts: '{{inventory}}'
  become: yes
  roles:
  - install-software
```

## Default Settings

```
debug_enabled_default: false
```

## Usage

Install specified software:

```
ansible-playbook install-software.yml --extra-vars "inventory=rhel7" -i hosts-dev
```
