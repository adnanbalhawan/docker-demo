# ansible-role-connectivity-test

Use nc (netcat) to test connectivity for firewall requests (also generates traffic to see in firewall log), and test for internal network issues.  
Dynamically determines source IP to use based on destination IP.


Distros tested
------------

* CentOS 7.x
* CentOS 5.9


Dependencies
------------

OS commands:  
* nc (netcat) - will be installed if not present
* ip
* awk


Default Settings
------------

- Enable debug
```
debug_enabled_default: false
```
debug_enabled_default: false

- Timeout
```
connectivity_test_wait_default: 10
```

- Proxy
```
proxy_env: []
```


Example config file (group_vars/xxx/connectivity-test-vars.yml)
------------

```
---
connectivity_test_destinations:
  - { ip: 192.168.56.10, port: 22 }
  - { ip: 192.168.56.10, port: 5000 }
  - { ip: 192.168.56.13, port: 443 }
```

Example Playbook connectivity-test.yml
------------

``` 
---
- hosts: '{{inventory}}'
  become: yes
  roles:
  - connectivity-test
```

Usage
------------

```
ansible-playbook connectivity-test.yml --extra-vars "inventory=all-dev" -i hosts
```

## Example output

```
TASK [setup] *******************************************************************
ok: [centos5]
ok: [centos7]

TASK [connectivity-test : Install dependant packages in RedHat based machines] *
ok: [centos5] => (item=[u'nc'])
ok: [centos7] => (item=[u'nc'])

TASK [connectivity-test : Get source IP based on destination IP] ***************
ok: [centos7] => (item=source ip: 192.168.56.13, destination ip: 192.168.56.10)
ok: [centos5] => (item=source ip: 192.168.56.10, destination ip: 192.168.56.10)
ok: [centos7] => (item=source ip: 192.168.56.13, destination ip: 192.168.56.10)
ok: [centos5] => (item=source ip: 192.168.56.10, destination ip: 192.168.56.10)
ok: [centos7] => (item=source ip: 192.168.56.13, destination ip: 192.168.56.13)
ok: [centos5] => (item=source ip: 192.168.56.10, destination ip: 192.168.56.13)

TASK [connectivity-test : debug] ***********************************************

TASK [connectivity-test : test network connectivity] ***************************
ok: [centos7] => (item=source ip: 192.168.56.13, destination ip: 192.168.56.10, port: 22, time: 2017-02-22 13:21:23.044606, stdout: SSH-2.0-OpenSSH_4.3, stderr: Ncat: Version 6.40 ( http://nmap.org/ncat )
Ncat: Connected to 192.168.56.10:22.
Ncat: 0 bytes sent, 20 bytes received in 0.01 seconds.

)
ok: [centos5] => (item=source ip: 192.168.56.10, destination ip: 192.168.56.10, port: 22, time: 2017-02-22 13:21:22.498293, stdout: SSH-2.0-OpenSSH_4.3
Connection to 192.168.56.10 22 port [tcp/ssh] succeeded!, stderr: 

)
ok: [centos7] => (item=source ip: 192.168.56.13, destination ip: 192.168.56.10, port: 5000, time: 2017-02-22 13:21:23.307189, stdout: , stderr: Ncat: Version 6.40 ( http://nmap.org/ncat )
Ncat: Connection refused.

)
ok: [centos5] => (item=source ip: 192.168.56.10, destination ip: 192.168.56.10, port: 5000, time: 2017-02-22 13:21:22.824379, stdout: , stderr: nc: connect to 192.168.56.10 port 5000 (tcp) failed: Connection refused

)
ok: [centos7] => (item=source ip: 192.168.56.13, destination ip: 192.168.56.13, port: 443, time: 2017-02-22 13:21:23.574678, stdout: , stderr: Ncat: Version 6.40 ( http://nmap.org/ncat )
Ncat: Connection refused.

)
ok: [centos5] => (item=source ip: 192.168.56.10, destination ip: 192.168.56.13, port: 443, time: 2017-02-22 13:21:23.102524, stdout: , stderr: nc: connect to 192.168.56.13 port 443 (tcp) failed: No route to host

)

TASK [connectivity-test : debug] ***********************************************

PLAY RECAP *********************************************************************
centos5                    : ok=4    changed=0    unreachable=0    failed=0   
centos7                    : ok=4    changed=0    unreachable=0    failed=0   

```

Manual way
------------

Ref: http://wncwiki.int.bell.ca/mediawiki/index.php/Ryan%27s_Random_Tips#Generate_traffic_for_Firewall_request

```
dIP="10.0.0.16 10.0.0.17";
dPORT="1521";
sIP=$(/sbin/ip route get "$(echo $dIP|awk -F" " 'NR==1{print $1}')"|awk 'NR==1{print $NF}');
uname -n;
echo "Src IP: $sIP";
for i in $dIP;
do date;
nc -v -w 3 -s "$sIP" "$i" "$dPORT";
echo " ";
done
```

## TODO
add flag to change protocol? tcp default (no flag), -u = udp, --sctp = sctp (not in < RHEL7)