# ansible-role-install-trusted-cert

CentOS7 / RHEL7 - copy certificate into trusted root store.

## Example Playbook install-trusted-cert.yml

```
---
- hosts: '{{inventory}}'
  become: yes
  roles:
  - install-trusted-cert
```


## Default Settings

```
debug_enabled_default: false
install_cert_pkg: ca-certificates
install_cert_source:
  - bell_issuing_ca_4.cer
  - bell_root_ca.cer
install_cert_destination: /etc/pki/ca-trust/source/anchors
```


## Usage

Copy and enable trusted cert:
```
ansible-playbook install-trusted-cert.yml --extra-vars "inventory=centos7" -i hosts-dev
```


## Misc postfix/OS commands:
```
Goto internal website and save cert as .crt/.cer
yum install ca-certificates
cp bell_issuing_ca_4.cer /etc/pki/ca-trust/source/anchors
update-ca-trust extract

#No more errors about insecure cert:
curl -v https://git.vas.int.bell.ca
```
