export HISTSIZE=10000
export HISTFILE=~/.bash_history
export HISTTIMEFORMAT="%F %T "
export HISTCONTROL=ignoredups
export PROMPT_COMMAND="history -a" # Appends immediately to history
