# ansible-role-new-server-config

CentOS7 / RHEL7 - Misc new server config

Task modifies server. Typically this role runs first (before yum repo exists), so package installs will not work here.

This role makes below changes:

Modify sshd config:
* Remove ecdsa_key
* PermitRootLogin no
* UseDNS no
* PasswordAuthentication no

Warning: Don't lock yourself out of the server! Make sure you have ssh keys configured if you disable PasswordAuthentication.

Change sudo config to remove requiring tty.

Modify bash history config.

Remove hard-coded entry in /etc/resolv.conf - Workaround for RHEL7.5u3 image hard coded DNS entry (RHEL only)

Stop & Disable rpcbind service & socket. Security risk - not needed if not using NFS (RHEL only)


## Example Playbook new-server-config.yml

```
---
- hosts: '{{inventory}}'
  become: yes
  roles:
  - new-server-config
```


## Default Settings

```
debug_enabled_default: false
new_server_manage_bash_history: true
new_server_manage_sudo: true
new_server_manage_rpcbind: true
new_server_manage_ssh: true
#new_server_ssh_nodns: "yes|no"
new_server_ssh_usedns: "no"
#new_server_ssh_password: "yes|no"
new_server_ssh_password: "no"
#new_server_ssh_as_root: "yes|no"
new_server_ssh_as_root: "no"
```


## Usage

Copy and enable trusted cert:
```
ansible-playbook new-server-config.yml --extra-vars "inventory=centos7" -i hosts-dev
```


## Misc postfix/OS commands:
```

```
