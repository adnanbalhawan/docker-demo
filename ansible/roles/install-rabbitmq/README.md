# ansible-role-install-rabbitmq

RHEL7 / CentOS - Install and configure RabbitMQ


## Example Playbook install-rabbitmq.yml

```
---
- hosts: '{{inventory}}'
  become: yes
  roles:
  - install-rabbitmq
```


## Default Settings

```
debug_enabled_default: false
rabbitmq_version: 3.6.12
erlang_cookie: sweet_cookie
rabbitmq_user: rabbitmq
rabbitmq_group: rabbitmq
rabbitmq_limitnofiles: 65536
rabbitmq_mgmt_admin_user: admin
rabbitmq_mgmt_admin_pass: admin
rabbitmq_mgmt_monitor_user: guest
rabbitmq_mgmt_monitor_pass: guest
rabbitmq_packages:
  - sysstat
  - curl
  - erlang
  - net-tools
epel_rpm_url: "http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm"
epel_yum: "epel-release"
```


## Usage

Install and configure rabbitmq:
```
ansible-playbook install-rabbitmq.yml --extra-vars "inventory=rhel7" -i hosts-dev
```


## Misc OS commands

From: https://www.rabbitmq.com/install-rpm.html#kernel-resource-limits

```
Get limit of open files:
cat /proc/$(pidof beam.smp)/limits
```
