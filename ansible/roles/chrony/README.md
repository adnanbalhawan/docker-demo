# ansible-role-chrony

CentOS7 / RHEL7 uses Chrony by default, not ntpd.
Set Chrony to use local NTP servers. And stops/disables the old ntpd service.

This can also sets timezone.

Example Playbook chrony.yml
------------

```
---
- hosts: '{{inventory}}'
  become: yes
  roles:
  - chrony
```


Default Settings
------------

```
- debug_enabled_default: false
- chrony_ntpservers:
    - "172.25.155.225"
    - "172.25.166.66"
- timezone: America/Toronto
```
172.25.155.225 = 70QEB

172.25.166.66 = Technoparc

Usage
------------

```
Both Chrony(NTP) and change timezone
ansible-playbook chrony.yml --extra-vars "inventory=centos7" -i hosts-dev

Only chrony
ansible-playbook chrony.yml --extra-vars "inventory=centos7" --tags "chrony" -i hosts-dev

Only timezone
ansible-playbook chrony.yml --extra-vars "inventory=centos7" --tags "timezone" -i hosts-dev

```

Misc chrony/OS commands:
```
timedatectl
chronyc tracking;chronyc sources;chronyc sourcestats
```
