# ansible-role-install-email

CentOS7 / RHEL7 uses postfix by default, not sendmail.  
Set postfix to use local mailhost as defined in /etc/hosts.  

Or if cannot use Postfix (for example if you use community version of mysql) then use SSMTP. Also stops/disables the other email service (postfix or vice versa).  

Default is to use Postfix.  

Make sure you supply your own email relay server!  

Example Playbook install-email.yml
------------

```
---
- hosts: '{{inventory}}'
  become: yes
  roles:
  - install-email
```


Default Settings
------------

```
- debug_enabled_default: false
- install_email_mailhost: 192.168.10.100 mailhost
- install_email_pkg: postfix|ssmtp
- install_email_postfix_config: |
    relayhost = mailhost
    smtp_host_lookup = native
    disable_dns_lookups = yes
    myhostname = "{{ ansible_fqdn }}"
install_email_ssmtp_config: mailhub=mailhost
```


Usage
------------

Install and configure Postfix (and disable SSMTP if present)
```
ansible-playbook install-email.yml --extra-vars "inventory=centos7" -i hosts-dev
```

Only Install Postfix
```
ansible-playbook install-email.yml --extra-vars "inventory=centos7 install_email_pkg=postfix" --tags "install" -i hosts-dev
```

Only Configure Postfix
```
ansible-playbook install-email.yml --extra-vars "inventory=centos7 install_email_pkg=postfix" --tags "configure" -i hosts-dev
```

Install and configure Postfix with different mail relay than in default yml. For lab mail relay:
```
ansible-playbook install-email.yml --extra-vars "inventory=centos7 install_email_mailhost='127.0.0.1 mailhost'" -i hosts-dev
```

Install and configure SSMTP (and disable Postfix if present)
```
ansible-playbook install-email.yml --extra-vars "inventory=centos7 install_email_pkg=ssmtp" -i hosts-dev
```

## Misc postfix/OS commands:
```
# Fix mail (postfix)
#https://www.certdepot.net/rhel7-configure-system-forward-email-central-mail-server/
#http://www.postfix.org/postconf.5.html#disable_dns_lookups
#https://userlinux.net/postfix-resolving-etchosts-entries.html
#Prefer to use "mailhost" in mail config and then map it to hosts file. In case IP changes only need to change it in hosts file.
#Be sure to test this, your milage may vary.
#Make sure mail relay is in /etc/hosts file (as mailhost if using that with postconf command)
cat /etc/hosts|grep mailhost
127.0.0.1         mail.changeme.int.bell.ca  mailhost

cp -rp /etc/postfix/main.cf /etc/postfix/main-orig.cf
postconf -e 'relayhost = mailhost'
postconf -e 'smtp_host_lookup = native'
postconf -e 'disable_dns_lookups = yes'

postfix check
postconf -n
systemctl restart postfix

echo test |mailx -s "$(uname -n) - $(date)" youremail@example.com
mailq
tail /var/log/maillog

# Troubleshoot email (postfix)
#If from hostname is not accurate (not in /etc/hosts?), try forcing hostname:
#http://www.postfix.org/postconf.5.html#myhostname
#postconf -e 'myhostname = testingHOSTNAME.domain'
#postfix check
#postconf -n
#systemctl restart postfix
#echo test |mailx -s "$(uname -n) - $(date)" youremail@bell.ca

# OPTIONAL 
# Alternative email (SSMTP)
#http://linuxpitstop.com/install-ssmtp-to-send-emails-to-gmail-and-office3655/
#because if using community mysql, postfix not an option: https://access.redhat.com/solutions/2143201

#Make sure mail relay is in /etc/hosts file (as mailhost)
cat /etc/hosts|grep mailhost
127.0.0.1         mail.changeme.int.bell.ca  mailhost

yum --nogpgcheck install ssmtp

alternatives --config mta
#select ssmtp

vi /etc/ssmtp/ssmtp.conf
#add:
mailhub=mailhost

```
