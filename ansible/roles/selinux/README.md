# ansible-role-selinux

CentOS7 / RHEL7 - Change SELinux mode and reboot server.  

Requires Ansible 2.7 (uses new reboot module).  
Use Branch "PreAnsible27" for old reboot method.  

SELinux modes:
* enforcing
* permissive
* disabled


## Example Playbook selinux.yml

```
---
- hosts: '{{inventory}}'
  become: yes
  roles:
  - selinux
```


## Default Settings

```
debug_enabled_default: false
reboot_default: true
set_selinux_state: permissive
```


## Usage

Change SELinux mode to permissive and reboot server (default options):
```
ansible-playbook selinux.yml --extra-vars "inventory=dev-server" -i hosts-dev
```

Change SELinux mode to disabled and do NOT reboot server:
```
ansible-playbook selinux.yml --extra-vars "inventory=dev-server set_selinux_state=disabled reboot_default=false" -i hosts-dev
```
