Satellite Registration Ansible Role
===================================

This Ansible role is used to register hosts to the Satellite 6 infrastructure.

Before you start
===================================

The following steps must be followed:
* Firewall between your subnet and the Satellite servers must be opened. [Information](firewall.md)
* Communication goes through the back channel. Make sure that the appropriate route is set in your server.
    * For Montreal server, set route to XXX through your backchannel interface.
    * For Toronto server, set route to XXX through your backchannel interface.

The following knowledge is required:
* Ansible, Inventory and Ansible Roles
    * [Ansible Tutorial](https://serversforhackers.com/an-ansible-tutorial)
    * [Ansible Inventory](http://docs.ansible.com/ansible/intro_inventory.html)
    * [Ansible Roles tutorial](https://www.digitalocean.com/community/tutorials/how-to-use-ansible-roles-to-abstract-your-infrastructure-environment)
    * [Ansible Roles official documentation](http://docs.ansible.com/ansible/playbooks_roles.html)
* Git and Git Submodule
    * [Git tutorial with hands-on](https://try.github.io/levels/1/challenges/1)
    * [Git Submodule documentation](https://github.com/blog/2104-working-with-submodules)
    * [Git Submodule official documentation](https://git-scm.com/book/en/v2/Git-Tools-Submodules)

The following operating systems are supported:
* CentOS 6 and later
* Red Hat 5.9 and later

Installation
===================================
### Add satellite_register role to your GIT project

    > git init
    > git submodule add git@yourserver:ansible-roles/satellite-register.git roles/satellite-register

### Add role to playbook

See example section below.

### Update your Inventory file

#### Locations

Setting the location will define to which Satellite server the server will connect to.

Your inventory file must contain a location group. The default location is Montreal.

    Add the following groups in your inventory file:

    [montreal:children]
    # List of servers that are located in Montreal

    [toronto:children]
    # List of servers that are located in Toronto

### Manual registration

For manual registration documentation, check this page: [Manual registration](register.md)

This section contains the instructions on how to install or access the project. It could link to the web app or give concise instructions showing how to get up and running.

Examples
===================================

### Sample playbook

    ---
    - name: MAIN | Register server to satellite
      hosts: all
      become: true
      roles:
      - satellite-register

### Sample inventory file

    ...
    [montreal:children]
    e0100myserver1
    e0100myserver2
    e0100myserver3

    [toronto:children]
    w0100myserver1
    w0100myserver2
    w0575myserver3


[Bugs and Issues](#bugs-and-issues)
===================================

### Docker service restarting

Installing or updating the Katello CA certificate will trigger a restart on the Docker service. This is a **bug** in how the certificate is managed.

**Fix:** Fixed in RedHat Satellite 6.3 Katello CA package. Do not use old package!

**Workaround:** Do not execute this script on all your Docker servers at once.

### Connection to Satellite server timing out or failing

#### [Configured proxy](#configured-proxy)

This is usually caused by proxy setting. Looks for the following configurations.

    # There should be no proxy set in yum.conf
    grep -i proxy /etc/yum.conf
    <empty output expected>

    # The following lines should not have any setting or be commented:
    grep -i proxy /etc/sysconfig/rhn/up2date
    <expected output>
    enableProxy[comment]=Use a HTTP Proxy
    enableProxy=0
    proxyPassword[comment]=The password to use for an authenticated proxy
    proxyPassword=
    proxyUser[comment]=The username for an authenticated proxy
    proxyUser=
    httpProxy[comment]=HTTP proxy in host:port format, e.g. squid.redhat.com:3128
    httpProxy=
    enableProxyAuth[comment]=To use an authenticated proxy or not
    enableProxyAuth=0


    grep -i proxy /etc/rhsm/rhsm.conf
    <expected output>
    # an http proxy server to use
    proxy_hostname =
    # port for http proxy server
    proxy_port =
    # user name for authenticating to an http proxy, if needed
    proxy_user =
    # password for basic http proxy auth, if needed
    proxy_password =


    env | grep -i proxy
    <http_proxy and https_proxy should not be defined>

### Validation failed: Name is invalid, Name is invalid

This means that the hostname of the server and the network interface identifier in /etc/hosts are different.

Source: https://access.redhat.com/solutions/2835771

    hostname        someservername
    hostname -f     differentname

To fix it: Replace the value in the following file with the value of the `hostname` command.

    vi /etc/rhsm/facts/katello.facts
    {"network.hostname-override":"someservername"}

### Name or service not known

There is corruption in the name resolution of the yum program.

To fix it:

    # In Montreal
    cd
    wget http://yourserver/pub/katello-ca-consumer-latest.noarch.rpm
    yum install -y katello-ca-consumer-latest.noarch.rpm
    # In Toronto
    cd
    wget http://yourserver/pub/katello-ca-consumer-latest.noarch.rpm
    yum install -y katello-ca-consumer-latest.noarch.rpm

If wget still doesn't work, that means there can be a network misconfiguration on your system.

### Can't install Katello-agent
Here is the output you get when you try to install katello-agent:

    # yum install katello-agent
    Loaded plugins: product-id, security, subscription-manager
    This system is receiving updates from Red Hat Subscription Management.
    ...
    Setting up Install Process
    No package katello-agent available.
    Error: Nothing to do

This is caused by the fact that the subscription-manager package on your system is too old. Run the following commands to update it:

    yum update subscription-manager -y
    subscription-manager refresh
    yum clean all
    yum repolist
