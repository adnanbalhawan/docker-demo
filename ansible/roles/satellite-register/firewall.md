Satellite server firewall connectivity
==================================

# Satellite servers

    # Use the one that is closest to your server location
    # In Montreal
    yourserver   172.25.x.x
    # In Toronto
    yourserver   172.25.x.x

# Test connectivity

Here are a few methods to test the connectivity:

## Using telnet

    telnet <satellite fqdn or ip> 8443

      Trying 172.25.x.x...
      Connected to 172.25.x.x.
      Escape character is '^]'.

## Installing the Katello CA certificate

### If your server cannot resolve the Satellite FQDN, add it to your host file

    vi /etc/hosts
    # If server is in Montreal:
    172.25.x.x  yourserver   yourserver-v
    # If server is in Toronto:
    172.25.x.x  yourserver2   yourserver2-v

### Install certificate

    # In Montreal
    yum install -y http://yourserver/pub/katello-ca-consumer-latest.noarch.rpm
    # In Toronto
    yum install -y http://yourserver/pub/katello-ca-consumer-latest.noarch.rpm

# Firewall request

If the connection fails, this means that you must open a [DNO firewall request]

Here is the port information. Follow all the other steps in the request itself.

    # Montreal
    Source IP           Destination IP    Port                                  Bi-Direction
    <Your subnet>       172.25.x.x        TCP 80, 5647, 443, 8000, 8140, 8443   Not needed
    <Your subnet>       172.25.x.x        UDP 67, 68, 69                        Not needed

    # Toronto
    Source IP           Destination IP    Port                                   Bi-Direction
    <Your subnet>       172.25.x.x        TCP 80, 5647, 443, 8000, 8140, 8443   Not needed
    <Your subnet>       172.25.x.x        UDP 67, 68, 69                        Not needed

    Subnet example: 172.25.x.0/24 (This will allow all servers in the subnet to connect with Satellite)
