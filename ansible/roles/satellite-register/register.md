Manual registration steps
===================================

If your system doesn't support Ansible, here is how you can manually register your server.

Before you start
===================================

The following steps must be followed:
* Firewall between your subnet and the Satellite servers must be opened. [Information](firewall.md)
* Communication goes through the back channel. Make sure that the appropriate route is set in your server.
    * For Montreal server, set route to XXX through your backchannel interface.
    * For Toronto server, set route to XXX through your backchannel interface.


The following operating systems are supported:
* CentOS 6 and later
* Red Hat 5.9 and later

Installation
===================================

### Hostfile

If your server cannot resolve DNS names, you must add the Satellite server entry to your host file.

    vi /etc/hosts
    # If server is in Montreal:
    172.25.x.x  yourserver   yourserver-v
    # If server is in Toronto:
    172.25.x.x  yourserver2   yourserver2-v

### Install Satellite certificate

    # In Montreal
    yum install -y http://yourserver/pub/katello-ca-consumer-latest.noarch.rpm
    # In Toronto
    yum install -y http://yourserver/pub/katello-ca-consumer-latest.noarch.rpm

### Install SeLinux dependencies

    yum install -y libselinux-python

### Make sure subscription-manager is installed and latest version

    yum install -y subscription-manager
    yum update -y subscription-manager

### Remove proxy configuration for Red Hat Subscription manager

See [README.md](README.md#configured-proxy)

### Remove RHN packages (old Red Hat subscription application)

    yum erase -y rhn-setup rhn-client-tools yum-rhn-plugin rhnsd rhn-check rhnlib spacewalk-abrt spacewalk-oscap osad rh-*-rhui-client

### Register to Satellite server

#### Activation keys
Activation keys have been defined to help groups servers and provide appropriate basic subscriptions.

Here is the list of activation keys:


#### Register to Satellite
    # Using your activation key (from the table above)
    subscription-manager register --org=VAS --activationkey=<activation-key>
    yum install -y katello-agent

#### Remove duplicate repositories
Red Hat Satellite 6 provides repositories for both Red Hat and EPEL.

    ls -l /etc/yum.repos.d
    # redhat.repo is the repository created by Subscription Manager for Red Hat and EPEL repositories
    # any other Red Hat or EPEL repository can be safely disabled or removed
    # do not touch other repositories as they may not be available through Satellite.
    # To disable, change the line enabled=1 to enabled=0
    # To remove it, delete the file (ex: rm epel.repo)

Bugs and Issues
===================================
See [README.md](README.md#bugs-and-issues)
