Red Hat Satellite Known issues:
==========
# SAM:
- Installation of the Katello CA Consumer certificate forces a restart of the **Docker** service

# Fixed issues:
- 2016/09/20: Errors related to rhn-setup package (Fixed in @ff86aad2)
- 2016/09/20: Katello-agent cannot be found (fixed by splitting activation keys per OS in Red Hat Satellite server)
- 2016/09/21: Subscription Manager cannot connect to Red Hat Satellite if proxy is configured (Fixed in @357c49e5)
