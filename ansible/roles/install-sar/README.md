# ansible-role-install-sar

CentOS7 / RHEL7 - Install and configure sar

Will run sar stats collection every minute (by default).


Example Playbook check-rhel.yml
------------

```
---
- hosts: '{{inventory}}'
  become: yes
  roles:
  - install-sar
```


Default Settings
------------

```
debug_enabled_default: false
#How often sar runs to collect stats (1 = 1 minute). RHEL default is 10. Can't go under 1.
install_sar_cron_interval: 1
```


Usage
------------

Install and configure sar:
```
ansible-playbook install-sar.yml --extra-vars "inventory=rhel7" -i hosts-dev
```
