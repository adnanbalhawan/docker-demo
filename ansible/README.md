# Geekfest Demo - Ansible

Uses Ansible playbooks to install/config servers and deploy a Docker Swarm.

## Usage

- Install ansible on jump box etc (done via bootstrap script)
- Terraform bootstrap script created ssh keys already for ansible user on ansible control server
- ssh to ansible control server

### OpenStack

SSH to the floating IP of the Ansible control server (jumpbox). This can be found in the OpenStack web dashboard or from the OpenStack CLI command:

```bash
openstack floating ip list

ssh centos@xxx.xxx.xxx.xxx
```

Change IP xxx.xxx.xxx.xxx to your floating IP assigned to the Ansible control server.  

### Laptop

Using Vagrant:

```bash
cd demo-docker-tech-stack/deploy-local-vagrant
vagrant ssh ansible
```

Or your own ssh terminal:

```bash
ssh centos@192.168.100.99
```

### Continue with Ansible

```bash

#wait for bootstrap script to finish. ~5 mins. tail -f /var/log/messages. Or wait for the broadcast message.
sudo grep "Terraform and bootstrap completed" /var/log/messages
sudo su - ansible

#Proxy is already setup for you. No need to export http_proxy...

git clone https://git.ctslab.int.bell.ca/wnc/geekfest-2019-docker-tech-stack.git demo-docker-tech-stack
cd demo-docker-tech-stack/ansible

echo "RANDOM PASSWORD HERE #TODO Change" > .vaultpass
```

- create ansible user on all clients (using ansible playbook "create-user-ansible")

```bash
ansible-playbook create-user-ansible.yml --become --ask-pass --become-method=sudo --extra-vars "inventory=swarm-hosts ansible_ssh_user=centos username=ansible" -i inventories/sandbox-openstack/hosts

#When prompted, enter password you set in Terraform cloud-init.conf OR Vagrant config ( Default is: Password123321123321! )
```

- Install/Configure/Patch the new servers:

You can use your own Playbooks here, but let's do the same thing as in Production!
This will:
Misc config like sshd
Configure SELinux
Patch/Update packages/kernel and reboot
Configure email
Configure NTP (Chrony)
Install Bell certificates
Create users and set root password (so you can get into Console!)

```bash
ansible-playbook new-server-swarm.yml --extra-vars "inventory=swarm-hosts" -i inventories/sandbox-openstack/hosts
```

- Deploy Docker Swarm:

If using OpenStack, use eth0:

```bash
# Use a symbolic link to the hosts file. This hack allows group_vars to work! Should be present but in case it's not. Or in case Windows broke the symbolic link when you copied the files, create it:
#ln -s inventories/sandbox-openstack/hosts hosts-sandbox

ansible-playbook ./roles/install-dockerV2/install-dockerV2.yml --extra-vars "swarm_iface=eth0" -i hosts-sandbox
```

If using Vagrant, use eth1:

```bash
ansible-playbook ./roles/install-dockerV2/install-dockerV2.yml --extra-vars "swarm_iface=eth1" -i hosts-sandbox
```

- Disable password authentication

If using OpenStack. Run on local (Ansible control server), and Docker Swarm servers:

```bash
ansible-playbook new-server-config.yml --extra-vars "inventory=local,swarm-hosts new_server_ssh_password=no" -i inventories/sandbox-openstack/hosts
```

If using Vagrant, no can leave password authentication enabled if you want. Only you can connect.

- DONE! Your Docker Swarm is ready to go.  
- [Continue to Setup Docker Swarm (using Stack files)](../docker-stack-files)

## Random tips

- to debug what internal ansible variables are set to:

```bash
ansible swarm-hosts -m setup -i inventories/sandbox-openstack/hosts
```

- You can run individual roles as needed:

```bash
ansible-playbook selinux.yml --extra-vars "inventory=swarm-hosts" -i inventories/sandbox-openstack/hosts
ansible-playbook server-update-reboot.yml --extra-vars "inventory=swarm-hosts" -i inventories/sandbox-openstack/hosts
ansible-playbook install-email.yml --extra-vars "inventory=swarm-hosts install_email_mailhost='127.0.0.1   mail.yourserver.int.bell.ca  mailhost'" -i inventories/sandbox-openstack/hosts
ansible-playbook chrony.yml --extra-vars "inventory=swarm-hosts" -i inventories/sandbox-openstack/hosts
ansible-playbook create-users-sandbox.yml --extra-vars "inventory=swarm-hosts" -i inventories/sandbox-openstack/hosts
```

### Not using a proxy with Vagrant (local laptop / homelab)

If you don't need to use a proxy when installing Docker and configuring Docker Swarm, add the variable to the command to disable the proxy:

```bash
ansible-playbook ./roles/install-dockerV2/install-dockerV2.yml --extra-vars "swarm_iface=eth1 install_docker_proxy_enabled=false" -i hosts-sandbox
```

### Create Users Role

This demo is setting passwords in [vars/create-users-sandbox.secret](vars/create-users-sandbox.secret). The Ansible Playbook is calling this file [new-server-swarm.yml](new-server-swarm.yml).  

[See README for the Create Users role for more info](roles/create-users)

### ansible-vault

Use ansible-vault to encrypt sensitive info from git.

Keep your Vault password safe (In your team's KeePass?). And don't forget to add it to the file .gitignore

Below is example that is needed by submodule "create-users".

```bash
cat vars/create-users-sandbox.secret
#Encrypt if cleartext (before git commit/push)
ansible-vault encrypt vars/create-users-sandbox.secret

#Edit encrypted file:
ansible-vault edit vars/create-users-sandbox.secret

vi .vaultpass
#Enter the password for Ansible Vault from your Password Safe
chmod 600 .vaultpass
vi ansible.cfg
#Insert the following lines
[defaults]
vault_password_file = ./.vaultpass
```

### How to generate Linux hashed password

- on Ubuntu - Install "whois" package

```bash
mkpasswd --method=SHA-512
```

- on RedHat - Use Python

```bash
python -c 'import crypt,getpass; print(crypt.crypt(getpass.getpass(), crypt.mksalt(crypt.METHOD_SHA512)))'
```
