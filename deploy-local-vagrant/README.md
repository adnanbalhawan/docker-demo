# Geekfest Demo - Vagrant & Laptop

Vagrant Demo that creates Ansible instance and some Docker Swarm servers.  
After Vagrant creates the servers, the bootstrap script installs Ansible.  

Note: You can't be on BELL1 wifi. After installing VirtualBox you will lose network connectivity. Use a wired connection or VPN or disable the VirtualBox Network Adapter. [See Limitations](../README.md#Disable%20Network%20Adapter%20when%20using%20BELL1%20wifi)

## Prep

### Install Git Bash (for Windows)

<https://git-scm.com/download/win>

### Install VirtualBox

<https://www.virtualbox.org/wiki/Downloads>

### Install Vagrant

<https://www.vagrantup.com/downloads.html>

### Install Vagrant plugins

On Windows, open git bash terminal:

```bash
export http_proxy=http://fastweb.int.bell.ca:80/
export https_proxy=http://fastweb.int.bell.ca:80/

vagrant plugin install vagrant-proxyconf
```

## Initialize Vagrant and this Demo Project

On Windows, open git bash terminal:

```bash
export http_proxy=http://fastweb.int.bell.ca:80/
export https_proxy=http://fastweb.int.bell.ca:80/

git clone https://git.ctslab.int.bell.ca/wnc/geekfest-2019-docker-tech-stack.git demo-docker-tech-stack

cd demo-docker-tech-stack/deploy-local-vagrant
```

- Ready to use Vagrant!

## Vagrant everything

You need to be in the directory with the Vagrantfile for Vagrant commands to work.

### Vagrant

Use Vagrant to create VMs of Ansible and Docker Swarm.  
On Windows, open Git Bash terminal:

```bash
vagrant up
```

You are now done using Vagrant. Continue to use Ansible to install / configure the Docker Swarm.

[Jump to next step](../ansible)

## Misc Tips

### Misc Vagrant commands

Most commands will be applied to all VMs in the folder. Or specify the VM name to apply to that specific VM.  

See [Vagrant documentation](https://www.vagrantup.com/docs/cli/) for list of all commands. Here are a few common commands.  

Only start Docker Swarm VMs (leader last to let workers complete startup)

```bash
vagrant up swarmwork-01 swarmwork-02 swarmlead1
```

Only stop Docker Swarm VMs (leader will stop first to prevent failovers)

```bash
vagrant halt swarmwork-01 swarmwork-02 swarmlead1
```

Show status of VirtualBox VMs created by Vagrant:

```bash
vagrant status
```

Show ssh config/connection info:

```bash
vagrant ssh-config
```

ssh into VM:

```bash
vagrant ssh [name]
```

Stop all VMs:

```bash
vagrant halt
```

Sync shared folders (/demo on VM). For example if you changed the stack file, on local laptop you need to run this to sync files to the VM.

```bash
vagrant rsync
```

Re-run Vagrantfile and perform bootstrap scripts again, etc:

```bash
vagrant provision
```

Restart VMs (reloading any provisioning changes):

```bash
vagrant reload
```

Delete all VMs:

```bash
vagrant destroy
```

Update Vagrant images (boxes):

```bash
vagrant box update
```

List Vagrant boxes:

```bash
vagrant box list
```

[Cleanup Vagrant boxes](https://www.vagrantup.com/docs/boxes/versioning.html#pruning-old-versions):

```bash
vagrant box prune
```

### See what VM is doing

```bash
top -bc1|head; docker stats --no-stream
```

### Not using a proxy

#### Existing VMs

If you already created the VMs but are now not using a proxy.. you'll need to go into each VM and disable where proxy is set, there's no easy way to switch back and forth. Edit:

```bash
/etc/yum.conf
/etc/systemd/system/docker.service.d/docker-proxy.conf

# Restart docker on all managers and workers
systemctl restart docker
```

#### New VMs

If you are creating new VMs, comment out anywere in these local files that have the proxy in it:

```bash
Vagrantfile
bootstrap-ansible-control.sh
tig-stack.yml
```

### SSH not using Vagrant / Git bash

Use your preferred ssh program. Get the port used for ssh, for the instance desired.

```bash
$ vagrant ssh-config
Host ansible
  HostName 127.0.0.1
  User vagrant
  Port 2200
  UserKnownHostsFile /dev/null
  StrictHostKeyChecking no
  PasswordAuthentication no
  IdentityFile C:/Users/username/coding/geekfest/deploy-local-vagrant/.vagrant/machines/ansible/virtualbox/private_key
  IdentitiesOnly yes
  LogLevel FATAL
```

In your own ssh program:

```bash
ssh -p 2200 vagrant@127.0.0.1
```

Default vagrant password: vagrant  
You could use the ssh key, but WSL (Windows Subsystem for Linux) mounts everything as 777 and ssh does not like this. You could copy the key somewhere local to WSL and: `chown 600 private_key`  

### How to generate linux hashed password

- on Ubuntu - Install "whois" package

```bash
mkpasswd --method=SHA-512
```

- on RedHat - Use Python2

```bash
python -c 'import crypt,getpass; print(crypt.crypt(getpass.getpass(), crypt.mksalt(crypt.METHOD_SHA512)))'
```
