################################################################################
# Demo Deploy Ansible and Docker Swarm #########################################
################################################################################

## Variables ###################################################################
### Network ####################################################################
# See main.tf

### Flavor Ansible #############################################################
variable "ansible_flavor_name" {
  type    = "string"
  default = "m1.small"
}

### Flavor Swarm Leader ########################################################
variable "swarmlead_flavor_name" {
  type    = "string"
  default = "m1.small"
}

### Flavor Swarm Worker ########################################################
variable "swarmwork_flavor_name" {
  type    = "string"
  default = "m1.medium"
}

### Image Ansible ##############################################################
variable "ansible_image_name" {
  type = "string"
#  default = "RHEL-7.6"
  default = "CentOS-7-1901" # TODO CHANGE BY USER
}

### Image Swarm Leader #########################################################
variable "swarmlead_image_name" {
  type = "string"
#  default = "RHEL-7.6"
  default = "CentOS-7-1901" # TODO CHANGE BY USER
}

### Image Swarm Worker #########################################################
variable "swarmwork_image_name" {
  type = "string"
#  default = "RHEL-7.6"
  default = "CentOS-7-1901" # TODO CHANGE BY USER
}

### Root disk size Swarm Leader ################################################
variable "swarmlead_root_disk_size" {
  type = "string"
  default = "10"
}

### Root disk size Swarm Worker ################################################
variable "swarmwork_root_disk_size" {
  type = "string"
  default = "30"
}

## Groups ######################################################################
resource "openstack_compute_servergroup_v2" "swarmlead_group" {
  name     = "swarmlead_group"
  policies = ["anti-affinity"]
}
resource "openstack_compute_servergroup_v2" "swarmwork_group" {
  name     = "swarmlead_group"
  policies = ["anti-affinity"]
}

## Resources ###################################################################
### Ansible control server #####################################################
resource "openstack_compute_instance_v2" "ansible-control" {
  name            = "ansible-01"
  image_name      = "${var.ansible_image_name}"
  flavor_name     = "${var.ansible_flavor_name}"
  key_pair        = "${var.os_key_pair}"
  user_data       = "${file("bootstrap-ansible-control.sh")}"
  security_groups = ["default"]

  block_device {
    uuid                  = "${openstack_blockstorage_volume_v1.ansible-control_root_disk.id}"
    source_type           = "volume"
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  network {
    name = "${var.management_network}"
    fixed_ip_v4 = "192.168.100.99"
  }

  config_drive = true
  stop_before_destroy = true

  lifecycle {
    ignore_changes = ["user_data"]
  }
}

resource "openstack_blockstorage_volume_v1" "ansible-control_root_disk" {
  name = "ansible-control_root_disk"
  image_id = "${data.openstack_images_image_v2.centos7_1901.id}"
  size     = 30
}

#### Assign floating IP ########################################################
resource "openstack_compute_floatingip_associate_v2" "fip_1" {
  floating_ip = "${openstack_networking_floatingip_v2.fip_1.address}"
  instance_id = "${openstack_compute_instance_v2.ansible-control.id}"
}

### Swarm Leader 1 #############################################################
resource "openstack_compute_instance_v2" "swarmlead-01" {
  name            = "swarmlead-01"
  image_name      = "${var.swarmlead_image_name}"
  flavor_name     = "${var.swarmlead_flavor_name}"
  key_pair        = "${var.os_key_pair}"
  security_groups = ["default"]
  user_data       = "${file("cloud-init.conf")}"
  scheduler_hints {
    group         = "${openstack_compute_servergroup_v2.swarmlead_group.id}"
  }

  block_device {
    uuid                  = "${openstack_blockstorage_volume_v1.swarmlead-01_root_disk.id}"
    source_type           = "volume"
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  network {
    name = "${var.management_network}"
    fixed_ip_v4 = "192.168.100.100"
  }

  config_drive = true
  stop_before_destroy = true

  lifecycle {
    ignore_changes = ["user_data"]
  }
}

resource "openstack_blockstorage_volume_v1" "swarmlead-01_root_disk" {
  name = "swarmlead-01_root_disk"
  image_id = "${data.openstack_images_image_v2.centos7_1901.id}"
  size     = "${var.swarmlead_root_disk_size}"
}

#### Assign floating IP ########################################################
resource "openstack_compute_floatingip_associate_v2" "fip_2" {
  floating_ip = "${openstack_networking_floatingip_v2.fip_2.address}"
  instance_id = "${openstack_compute_instance_v2.swarmlead-01.id}"
}

### Swarm Worker 1 #############################################################
resource "openstack_compute_instance_v2" "swarmwork-01" {
  name            = "swarmwork-01"
  image_name      = "${var.swarmwork_image_name}"
  flavor_name     = "${var.swarmwork_flavor_name}"
  key_pair        = "${var.os_key_pair}"
  security_groups = ["default"]
  user_data       = "${file("cloud-init.conf")}"
  scheduler_hints {
    group         = "${openstack_compute_servergroup_v2.swarmwork_group.id}"
  }

  block_device {
    uuid                  = "${openstack_blockstorage_volume_v1.swarmwork-01_root_disk.id}"
    source_type           = "volume"
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  network {
    name = "${var.management_network}"
    fixed_ip_v4 = "192.168.100.101"
  }

  config_drive = true
  stop_before_destroy = true

  lifecycle {
    ignore_changes = ["user_data"]
  }
}

resource "openstack_blockstorage_volume_v1" "swarmwork-01_root_disk" {
  name = "swarmwork-01_root_disk"
  image_id = "${data.openstack_images_image_v2.centos7_1901.id}"
  size     = "${var.swarmwork_root_disk_size}"
}

### Swarm Worker 2 #############################################################
resource "openstack_compute_instance_v2" "swarmwork-02" {
  name            = "swarmwork-02"
  image_name      = "${var.swarmwork_image_name}"
  flavor_name     = "${var.swarmwork_flavor_name}"
  key_pair        = "${var.os_key_pair}"
  security_groups = ["default"]
  user_data       = "${file("cloud-init.conf")}"
  scheduler_hints {
    group         = "${openstack_compute_servergroup_v2.swarmwork_group.id}"
  }

  block_device {
    uuid                  = "${openstack_blockstorage_volume_v1.swarmwork-02_root_disk.id}"
    source_type           = "volume"
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  network {
    name = "${var.management_network}"
    fixed_ip_v4 = "192.168.100.102"
  }

  config_drive = true
  stop_before_destroy = true

  lifecycle {
    ignore_changes = ["user_data"]
  }
}

resource "openstack_blockstorage_volume_v1" "swarmwork-02_root_disk" {
  name = "swarmwork-02_root_disk"
  image_id = "${data.openstack_images_image_v2.centos7_1901.id}"
  size     = "${var.swarmwork_root_disk_size}"
}

### Swarm Worker 3 #############################################################
# resource "openstack_compute_instance_v2" "swarmwork-03" {
#   name            = "swarmwork-03"
#   image_name      = "${var.swarmwork_image_name}"
#   flavor_name     = "${var.swarmwork_flavor_name}"
#   key_pair        = "${var.os_key_pair}"
#   security_groups = ["default"]
#   user_data       = "${file("cloud-init.conf")}"
#   scheduler_hints {
#     group         = "${openstack_compute_servergroup_v2.swarmwork_group.id}"
#   }

#   block_device {
#     uuid                  = "${openstack_blockstorage_volume_v1.swarmwork-03_root_disk.id}"
#     source_type           = "volume"
#     boot_index            = 0
#     destination_type      = "volume"
#     delete_on_termination = false
#   }

#   network {
#     name = "${var.management_network}"
#     fixed_ip_v4 = "192.168.100.103"
#   }

#   config_drive = true
#   stop_before_destroy = true

#   lifecycle {
#     ignore_changes = ["user_data"]
#   }
# }

# resource "openstack_blockstorage_volume_v1" "swarmwork-03_root_disk" {
#   name = "swarmwork-03_root_disk"
#   image_id = "${data.openstack_images_image_v2.centos7_1901.id}"
#   size     = "${var.swarmwork_root_disk_size}"
# }
