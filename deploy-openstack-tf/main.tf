# Set up variables #############################################################
## Pulled in from OS Environmental variables when sourcing "openrc.sh" #########
variable "os_user_name" {
  type = "string"
  default = ""
}

variable "os_password" {
  type = "string"
  default = ""
}

variable "os_key_pair" {
  description = "The ssh keypair to be used from OpenStack"
  default     = "rdaniels-rsa4" # TODO CHANGE BY USER
}

## external_network is the "Name" of your OpenStack external network, usually
## something like: iaas-tenant-backchannel-tsz
## Find the Name in the web gui under "Network -> Networks -> iaas-backchannel-tsz" (or something similar)
variable "external_network" {
  description = "The external network to be used"
  # default     = "iaas-tenant-backchannel-tsz" # TODO CHANGE BY USER
  default     = "floating-ip_pool01 - iaas-backchannel-tsz" # TODO CHANGE BY USER
}

## external_network_id is the "ID" of your OpenStack external network, usually
## something like: iaas-tenant-backchannel-tsz
## Find the ID in the web gui under "Network -> Networks -> iaas-backchannel-tsz" (or something similar)
variable "external_network_id" {
  description = "The external network id to be used"
  # default     = "aed0757f-6deb-44f8-bc6e-0d458ea0e765" # TODO CHANGE BY USER
  # default     = "4daefbd9-c15e-4512-9325-962a7d795e8a" # TODO CHANGE BY USER
  default     = "af84a1a8-6be5-40ec-a503-60b7ebc9a79d" # TODO CHANGE BY USER
}

variable "management_network" {
  description = "The network to be used"
  default     = "management_network" # TODO CHANGE BY USER (OPTIONAL)
}

### Root Disk Image ############################################################
data "openstack_images_image_v2" "centos7_1901" { # TODO CHANGE BY USER (OPTIONAL)
  name = "CentOS-7-1901" # TODO CHANGE BY USER (OPTIONAL)
  properties {
    checksum = "d1b7893ff2a6e31c185d6e4307f23f0c" # TODO CHANGE BY USER (OPTIONAL)
  }
}

# Configure the OpenStack Provider #############################################
## Make sure auth_url is manually set here to your OpenStack env. ##############
## Found in your openrc.sh file: "cat openrc.sh | grep OS_AUTH_URL" ############
provider "openstack" {
  user_name   = "${var.os_user_name}"
  password    = "${var.os_password}"
  tenant_name = "Wireless_rdaniels_sandbox_tsz" # Same as the project name # TODO CHANGE BY USER
  # auth_url    = "http://10.55.99.246:5000/v3" # TODO CHANGE BY USER (Simcoe)
  # auth_url    = "http://10.55.127.246:5000/v3" # TODO CHANGE BY USER (87 Ontario)
  auth_url    = "http://10.55.119.246:5000/v3" # TODO CHANGE BY USER (Canniff)
  domain_name = "default"
}
