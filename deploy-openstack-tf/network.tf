# Router config ################################################################
resource "openstack_networking_router_v2" "central_router" {
  name             = "central_router"
  external_network_id = "${var.external_network_id}"
}

# Management network ###########################################################
resource "openstack_networking_network_v2" "management_network" {
  name           = "management_network"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "management_subnet" {
  name            = "management_subnet"
  network_id      = "${openstack_networking_network_v2.management_network.id}"
  cidr            = "192.168.100.0/24"
  ip_version      = 4
  gateway_ip      = "192.168.100.1"
  enable_dhcp     = "true"
  dns_nameservers = ["142.117.3.250", "142.117.3.247"]

  allocation_pools {
    start = "192.168.100.10"
    end   = "192.168.100.200"
  }
}

resource "openstack_networking_router_interface_v2" "management_router_interface" {
  router_id = "${openstack_networking_router_v2.central_router.id}"
  subnet_id = "${openstack_networking_subnet_v2.management_subnet.id}"
}

resource "openstack_networking_floatingip_v2" "fip_1" {
#  pool = "iaas-tenant-backchannel-tsz"
  pool = "${var.external_network}"
  lifecycle { prevent_destroy = true }
}

resource "openstack_networking_floatingip_v2" "fip_2" {
#  pool = "iaas-tenant-backchannel-tsz"
  pool = "${var.external_network}"
  lifecycle { prevent_destroy = true }
}
