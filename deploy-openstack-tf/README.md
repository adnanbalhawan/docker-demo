# Geekfest Demo - Terraform & OpenStack

Terraform Demo that creates Ansible jumpbox (with Floating IP) and some Docker Swarm servers.  
After terraform creates the servers, the bootstrap script installs Ansible.  

Terraform can easily run on Windows Subsystem for Linux (WSL).  

## Prep

### OpenStack Sandbox Enironment

#### Create OpenStack Sandbox

* Go to OpenStack self-serve page: <https://selfserve.stratus.int.bell.ca/>
* Create a new sandbox project, enter your details

  Project Name: BellNTid_sandbox  
  Seczone: trusted (since this demo is not internet facing or containing sensitive data. Change this if needed. But keep in mind to change anything with "tsz" in the Terraform files)  
  Site: Pick any  

* Login to your new OpenStack Sandbox

#### Upload CentOS image

* Upload CentOS Image. Download it from: <https://cloud.centos.org/centos/7/images/>

  Most recent available currently is:  
  CentOS-7-x86_64-GenericCloud-1901.qcow2  
  Upload it to OpenStack, in Web GUI -> Compute -> Images -> Create Image  
  Name: CentOS-7-1901  
  Format: Raw  
  Visibility: Private  

**After** the image is done uploading, the pop-up window will disappear. However it could say "Saving..." for a while, just reload the page.

#### Upload your SSH key pair

Go to:

`"Compute" -> "Access & Security" -> "Key Pairs"`

If you already have an ssh key, click: "Import Key Pair".  
If not, click: "Create Key Pair". Download it and add it into your .ssh directory.

Name it whatever you want, but remember the name, you will need it later.

This will be used to connect (via ssh) to all of your OpenStack instances.

#### Download OpenStack RC File v3

Download the "Download OpenStack RC File v3" from the OpenStack web gui. You can find it at:

`Compute -> Access & Security -> API Access -> Download OpenStack RC File v3`

List of all OpenStack web gui dashboards: <https://confluence.wnst.int.bell.ca/display/STRAT/List+of+OpenStack+dashboards>

Direct link for 87 Ontario Cloud OpenStack: <https://dashboard-az1.montreal42.stratus.int.bell.ca/horizon/project/access_and_security/?tab=access_security_tabs__api_access_tab>

Direct link for Canniff Cloud OpenStack: <https://dashboard-az1.toronto12.stratus.int.bell.ca/horizon/project/access_and_security/?tab=access_security_tabs__api_access_tab>

Direct link for Simcoe Cloud OpenStack: <https://dashboard-az1.w0569.stratus.int.bell.ca/horizon/project/access_and_security/?tab=access_security_tabs__api_access_tab>

Direct link for Belmont Cloud OpenStack: <https://dashboard-az1.f1252.stratus.int.bell.ca/horizon/project/access_and_security/?tab=access_security_tabs__api_access_tab>

Then upload to your Terraform server. (more on that below)

#### External Network details

Get id of the external network. You can find it at:

`Network -> Networks -> "floating-ip_pool01 - iaas-backchannel-tsz" (Or whatever is present for you)`

Make note of the "Name" and "ID".  
For me, "Name" is "floating-ip_pool01 - iaas-backchannel-tsz" and ID is 4daefbd9-c15e-4512-9325-962a7d795e8a  

## Initialize Terraform and this Demo Project

### Setup - On a remote server

```bash
sudo yum install wget unzip
sudo useradd -c 'Terraform User' -m -d /home/terraform -s /bin/bash terraform
passwd terraform

su - terraform
```

### Setup - On Windows Subsystem for Linux (WSL)

You can use your normal username.

Just make sure you can reach the OpenStack API IP & port. You can find this information in the web gui:

`"Compute" -> "Access & Security" -> "API Access" -> Identity`

### Download and initialize Terraform

```bash
export http_proxy=http://fastweb.int.bell.ca:80/
export https_proxy=http://fastweb.int.bell.ca:80/

wget https://releases.hashicorp.com/terraform/0.11.11/terraform_0.11.11_linux_amd64.zip
unzip -qq terraform_0.11.11_linux_amd64.zip

# Optional, to be able to run terraform command anywhere (this should put it in your PATH):
#sudo cp -rp terraform /usr/local/bin/

git clone https://git.ctslab.int.bell.ca/wnc/geekfest-2019-docker-tech-stack.git demo-docker-tech-stack

cd demo-docker-tech-stack/deploy-openstack-tf


# Initialize Terraform. It will automatically download OpenStack plugin for Terraform. (That's why we need the proxy again)

export http_proxy=http://fastweb.int.bell.ca:80/
export https_proxy=http://fastweb.int.bell.ca:80/

terraform init

unset http_proxy
unset https_proxy

# Ready to Terraform!
```

* Upload/Save the "OpenStack RC File v3" that you downloaded from the OpenStack web gui to your terraform server or local directory with the terraform demo project.  

* Edit main.tf, at the bottom of the file. You need to change the **tenant_name** and **auth_url** to what is in your "OpenStack RC File v3"

* Edit main.tf. Line 15. Change the variable "os_key_pair" to your ssh key pair name that you saved in OpenStack.

* Edit main.tf, line 23. Change external_network to the details you obtained above in "External Network details" section.

* Edit main.tf, line 31. Change external_network_id to the details you obtained above in "External Network details" section.

## Terraform everything

### Source OpenStack environment variables

Replace filename with your own file, and:

```bash
source Wireless_your_sandbox_tsz-openrc.sh
```

Don't forget to unset proxy before running terraform CLI commands:

```bash
unset http_proxy
unset https_proxy
```

### Terraform

See what will happen, apply no changes:

```bash
terraform plan
```

Apply changes and Terraform:

```bash
terraform apply
```

* Remember, save your terraform.tfstate files! If multiple people are working on the same environment this file needs to be saved in a central location, like git!

You are now done Terraforming. Continue to use Ansible to install / configure the Docker Swarm.

[Jump to next step](../ansible)

## Misc Issues

### Network access into Openstack

You will need a way to get into OpenStack. The web gui, and your floating IP(s) via SSH.  
From Bell1 WIFI, and VPN I've found the OpenStack team has already opened firewall rules to most OpenStack environments. However your milage may vary. You might need to open a firewall request.

## Optional

### Install OpenStack CLI

Install the OpenStack CLI. Info from: <https://docs.openstack.org/newton/user-guide/common/cli-install-openstack-command-line-clients.html>

This is for WSL (Ubuntu):

```bash
export http_proxy=http://fastweb.int.bell.ca:80/
export https_proxy=http://fastweb.int.bell.ca:80/

sudo apt install python-dev python-pip
pip install python-openstackclient

unset http_proxy
unset https_proxy
```

If pip isn't happy with the proxy, try directly using proxy in the pip install:

```bash
pip install --proxy http://fastweb.int.bell.ca:80 python-openstackclient
```

There's a bug in openstack on WSL/Ubuntu. You can't go into the interactive shell just by typing "openstack". Instead type everything outside the interactive shell, like: "openstack --help"

If on WSL (Windows Subsystem for Linux), for me sudo apt install doesn't work, always returns 404 errors. "sudo -i" first, then run apt install commands.

## OpenStack Security Rules

By default OpenStack doesn't allow much incomming traffic into your instances. To check and add new rules, go to:

`Compute -> Access & Security`  
`default -> Manage Rules`

You might need to allow ingress SSH traffic.  

## Command cheat sheet

### Misc Terraform / OpenStack commands

Don't forget to unset proxy before running openstack CLI commands:

```bash
unset http_proxy
unset https_proxy
```

Taint an OpenStack resource. This will destroy the resources and re-create them:

```bash
terraform taint openstack_compute_instance_v2.ansible-control
terraform taint openstack_blockstorage_volume_v1.ansible-control_root_disk
```

List OpenStack instances:

```bash
openstack server list
```

List OpenStack floating IP info:

```bash
openstack floating ip list
```

List OpenStack network(s). With a newly created project, only one external network will exist.

```bash
openstack network list --external
+--------------------------------------+-------------------------------------------+--------------------------------------+
| ID                                   | Name                                      | Subnets                              |
+--------------------------------------+-------------------------------------------+--------------------------------------+
| 4daefbd9-c15e-4512-9325-962a7d795e8a | floating-ip_pool01 - iaas-backchannel-tsz | 3288fb3a-b17d-4b51-95fc-a84d92fe3951 |
+--------------------------------------+-------------------------------------------+--------------------------------------+
```

## How to generate linux hashed password

* on Ubuntu - Install "whois" package

```bash
mkpasswd --method=SHA-512
```

* on RedHat - Use Python2

```bash
python -c 'import crypt,getpass; print(crypt.crypt(getpass.getpass(), crypt.mksalt(crypt.METHOD_SHA512)))'
```
