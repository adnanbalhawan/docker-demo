#!/bin/bash

if ! $(grep -q "^proxy" /etc/yum.conf); then echo 'proxy=http://fastweb.int.bell.ca:80' | tee -a /etc/yum.conf;fi
if ! $(grep -q "^export http_proxy" /etc/profile); then echo 'export http_proxy=http://fastweb.int.bell.ca:80' | tee -a /etc/profile;fi
if ! $(grep -q "^export https_proxy" /etc/profile); then echo 'export https_proxy=http://fastweb.int.bell.ca:80' | tee -a /etc/profile;fi
if ! $(grep -q "^export no_proxy" /etc/profile); then echo 'export no_proxy=localhost,127.0.0.1,localaddress,.bell.ca,.int.bell.ca' | tee -a /etc/profile;fi

#TODO: get result ($?) of each yum/pip install and retry (for loop) if not 0 (success)
yum install -y epel-release
yum install -y ansible gcc openssl-devel python-pip python-wheel python-virtualenv python-virtualenvwrapper git httpd-tools
pip --proxy http://fastweb.int.bell.ca install --upgrade pip==9.0.3
pip --proxy http://fastweb.int.bell.ca install python-openstackclient

# TODO CHANGE BY USER: change root password of password123321123321
# TODO CHANGE BY USER: change ansible password on server after install

echo "root:password123321123321" | chpasswd
useradd -c 'Ansible User' -m -d /home/ansible -s /bin/bash -p '$6$A7JV34osNUF2b9zh$/xvxo8mIOpIFo3l6SwJUzoTOKUv6ZVMHWQZzHf3eY/4mfdrc5NvMUCeezVCJIebzPhRhn71e8SDuNBMyhtAqv/' ansible
# Create ssh key for ansible user
ssh-keygen -t rsa -b 4096 -f ~/.ssh/id_rsa-ansible -N "" -C ansible@ansible-sandbox
mkdir -p /home/ansible/.ssh
chmod 700 /home/ansible/.ssh
cp ~/.ssh/id_rsa-ansible /home/ansible/.ssh/id_rsa
cp ~/.ssh/id_rsa-ansible.pub /home/ansible/.ssh/id_rsa.pub
chmod 600 /home/ansible/.ssh/id_rsa
chmod 644 /home/ansible/.ssh/id_rsa.pub

# Create ssh config for no StrictHostKeyChecking:
cat > /home/ansible/.ssh/config << 'EOF'
Host *
#       IdentityFile    ~/.ssh/id_rsa_ansible
        StrictHostKeyChecking no
        UserKnownHostsFile=/dev/null
EOF

chown -R ansible.ansible /home/ansible/.ssh

echo "ansible ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/99-ansible

# Increase bash history
cat > /etc/profile.d/bash-history-timestamps.sh << 'EOF'
export HISTSIZE=20000
export HISTFILE=~/.bash_history
export HISTTIMEFORMAT="%F %T "
export HISTCONTROL=ignoredups
export PROMPT_COMMAND="history -a" # Appends immediately to history
EOF

# Allow ssh passwords. Insecure and should be disabled after! # TODO CHANGE BY USER
sed -i 's/^PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
#echo -e "\nPermitRootLogin yes" >> /etc/ssh/sshd_config
systemctl restart sshd

# Check for yum failure because sometimes the proxy or repo gives a 404 and fails..
if $(grep "cloud-init: " /var/log/messages|grep -q fail); then
  fail_msg="Terraform and bootstrap completed. Error during yum install, check /var/log/messages for details"
  wall -n "$fail_msg"
  logger "$fail_msg"
else
  success_msg="Terraform and bootstrap completed"
  wall -n "$success_msg"
  logger "$success_msg"
fi

# TODO CHANGE BY USER: run "yum upgrade -y" and reboot ansible control server
#yum upgrade -y
#shutdown -r now
